import random
import pprint
import argparse
import io

#Constants
ARDUINO = 'Arduino'
ANDROID = 'Android'
IMPLEMENTACAO = 'Integração/Documentação'
RPI = 'RaspberryPi'
# to read from the export Excel File (as Unicode file)
#SEP = "\t" 
SEP = ";" 
FILE_ENC = "utf16"

# name and position on groups: TODO: Use just the name of the system
SYSTEMS = {ARDUINO:0,RPI:1,ANDROID:2,IMPLEMENTACAO:3}
NR_LEVELS = len(SYSTEMS)

def debugPrint(obj,desc='Obj'):
    if(DEBUG):
        print(desc,end=':')
        pprint.pprint(obj, compact=True)
        
testvalues= []

########################################
def getRandomStudent(system,level):
    """ 
    Get a random student for the specified system. ``level`` indicates the
    preference level that the result should returned from 
    """
    nrAvailable = len(systemChoices[system][level])
    if(nrAvailable == 0):
        debugPrint(systemChoices[system],"SystemChoices for "+system + " at level " + str(level))
        raise Exception("No more students for " + system )

    rnd = random.randrange(nrAvailable)
    testvalues.append(rnd)
    # get student for system within the current available preference level and remove it
    student = systemChoices[system][level].pop(rnd)
    if student not in studNotChosen: # student already assigned, call again
        try:
            return getRandomStudent(system, level)
        except Exception:
            raise # re-raise
    del (studNotChosen[student]) # remove as it has chosen
    return student

########################################
#TODO: remove flag from end of array...
def groupFilled(group):
    """
    Test if a group is already full. Returns ``True` or ``False```
    """
    if(group[len(SYSTEMS)]): # if already checked don't go through it
        return True
    for stud in group[:len(SYSTEMS)]: # don't check the flag at end
        if (not stud):
            return False

    group[len(SYSTEMS)] = True # flag it to speed up (not that much, but... :) )
    return True

########################################
def printGroup(group,grpNum):
    print("Grupo " + str(grpNum) + ",", end="")
    for system in sorted(SYSTEMS):
        print(group[SYSTEMS[system]] + ",", end="")
    #in case group has more elements
    if(len(group)> len(SYSTEMS)):
        for elem in group[len(SYSTEMS):]:
        #TODO: remove flag from end of array...
            if(type(elem) != bool): # ugly...
                print(elem + ",", end="")
    print()

# Initializations 
# **************
studChoices = {} 
"""
Indexed by name and contain array of ordered choices, choices are the keywords in SYSTEMS
Example: 
 {'Gomes': ['RaspberryPi', 'Arduino', 'Android', 'Implementacao'],
 'Guilherme': ['Arduino', 'Implementacao', 'RaspberryPi', 'Android'],
 'Gulliver': ['Android', 'RaspberryPi', 'Arduino', 'Implementacao'],
 'Joaquim': ['Arduino', 'Implementacao', 'RaspberryPi', 'Android'],
 'Julio': ['Implementacao', 'Arduino', 'RaspberryPi', 'Android'],
 'Kilo': ['Arduino', 'RaspberryPi', 'Implementacao', 'Android'],
 'Olika': ['Implementacao', 'Arduino', 'RaspberryPi', 'Android'],
 'Rui': ['Arduino', 'Implementacao', 'RaspberryPi', 'Android']}
"""

studNotChosen = {}
""" 
Students not yet chosen, dict with the name of the student. Starts with all the students, ends with students without groups
"""

systemChoices = {}
"""
the preferences per system, index is the name of the system and contains an array with elements for each preference level. Each level as a list of students that chose the system at that pref level
 Example: 
{'Android': [['Gulliver'],
             [],
             ['Gomes'],
             ['Joaquim', 'Guilherme', 'Kilo', 'Julio', 'Olika', 'Rui']],
 'Arduino': [['Joaquim', 'Guilherme', 'Kilo', 'Rui'],
             ['Julio', 'Olika', 'Gomes'],
             ['Gulliver'],
             []],
 'Implementacao': [['Julio', 'Olika'],
                   ['Joaquim', 'Guilherme', 'Rui'],
                   ['Kilo'],
                   ['Gulliver', 'Gomes']],
 'RaspberryPi': [['Gomes'],
                 ['Kilo', 'Gulliver'],
                 ['Joaquim', 'Guilherme', 'Julio', 'Olika', 'Rui'],
                 []]}
"""

# initialize systemChoices
for system in SYSTEMS:
    members = [[] for i in range(0,len(SYSTEMS))] 
    systemChoices[system] = members

# Read input to script
parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''Generating random groups based on preferences read from file.
     Input to -f should be a txt ; separated file with no headings (see SEP in script)
     Output is a csv file with a heading indicating the group allocation.

     If there are erros regarding encoding (see FILE_ENC in script):
     \t - file should be utf16 encoded with a BOM marker
     \t - in vim you can do:
     \t\t set fileencoding=utf16
     \t\t set bomb
     \t and then write the file'''
     )
parser.add_argument("-s", "--seed", default=12345678, type=int, help="set the seed for the random generator")
parser.add_argument("-d", "--debug", help="enable debug output", action="store_true")
parser.add_argument("-f", "--file", required=True, help="file to read with csv where order is Name, 1st choice, 2nd choice, etc.")
args = parser.parse_args()

DEBUG = args.debug

# Set seed 
random.seed(a=args.seed)
print("Using seed:" + str(args.seed))

# Read file input ***
print("Using file:" + args.file)
try:
    file = io.open(args.file,'r', encoding=FILE_ENC)
    txt = file.read() # read full file to mem
    file.close()
except FileNotFoundError:
    print("*** File not found:" + args.file + "\n\t Exiting")
    exit(2)

# Read preferences
# ****************
for line in sorted(txt.splitlines()):
    choices = list(map(str.strip,line.split(SEP)))
    if(len(choices) > 2): # empty line of not?
        studChoices[choices[0]] = choices[1:]
        debugPrint(choices)
        for pref in range(1,5):
            systemChoices[choices[pref]][pref-1].append(choices[0])
        studNotChosen[choices[0]] = True

numberGroups = len(studChoices)//4

#initialize groups
createdGroups = []
#TODO: remove flag from end of array...
for n in range(0,numberGroups):
    groupC = [False for i in range(0,len(SYSTEMS)+1)] # flag at end to see if filled
    createdGroups.append(groupC)

debugPrint (studChoices,"Student Choices at start")
debugPrint (systemChoices,"System Choices at start")



#choose first by level and then by system (all 1st options, then all 2nd options, etc.)
for l in range(0,NR_LEVELS):
    for n in range(0,numberGroups):
        if(groupFilled(createdGroups[n])):
            next
        for system in sorted(SYSTEMS):
            try:
                #only add a new student if none already allocated
                if(not createdGroups[n][SYSTEMS[system]]): # no student yet
                    student = getRandomStudent(system,l)
                    createdGroups[n][SYSTEMS[system]] = student
            except Exception: # no more students at this level, go for next system
                pass

debugPrint (createdGroups,"Groups Created at end")
debugPrint (testvalues,"values from random")
debugPrint (studNotChosen,"Student without groups")

print("Nº Grupo,",end="")
for system in sorted(SYSTEMS):
    print(system + ",",end="")
print()
for gNum in range(0,len(createdGroups)):
        printGroup(createdGroups[gNum],gNum)

# If remaining students are 3 create a group
if(len(studNotChosen) ==3):
    # re-initialize systemChoices
    systemChoices = {}
    for system in SYSTEMS:
        members = [[] for i in range(0, len(SYSTEMS))] 
        systemChoices[system] = members

    # add students not chosen's choices
    for student in studNotChosen:
        choices = studChoices[student]
        for pref in range(0,len(SYSTEMS)):
            systemChoices[choices[pref]][pref].append(student)
    extraGroup = [False for i in range(0,len(SYSTEMS)+1)]
    #choose only for some systems
    for l in range(0,NR_LEVELS):
        for system in sorted([ARDUINO, ANDROID, RPI]):
            try:
                #only add a new student if none already allocated
                if(not extraGroup[SYSTEMS[system]]): # no student yet
                    student = getRandomStudent(system,l)
                    extraGroup[SYSTEMS[system]] = student
            except Exception: # no more students at this level, go for next system
                pass
    extraGroup[SYSTEMS[IMPLEMENTACAO]] = "---"
    debugPrint(extraGroup,"Last group")
    printGroup(extraGroup,gNum+1)
else: # random choose a group for then
    print("x"*40)
    print("Students not yet allocated to groups")
    groupsWithExtra = {}
    for student in sorted(studNotChosen):
        rndGroup= random.randrange(0,len(createdGroups))
        while(rndGroup in groupsWithExtra):
            rndGroup= random.randrange(0,len(createdGroups))
        groupsWithExtra[rndGroup] = True
        print("Student:" + student+ " goes to group nr:" , rndGroup)
        newGroup = createdGroups[rndGroup][:] # create a copy
        newGroup.append(student)
        printGroup(newGroup,rndGroup)




